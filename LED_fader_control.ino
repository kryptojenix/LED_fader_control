// Pin Assignments - LED Indicators
const int statusLED = 13;
const int LEDpanel1 =  4;     // d out for panel LED1 red
const int LEDpanel2 =  3;     // d out for panel LED2 green
const int LEDpanel3 =  2;    // d out for panel LED3 blue


// Pin Assignments - Light Outputs
const int Light1 = 9;         // PWM output to MOSFET LED driver
const int Light2 = 10;         // PWM output to MOSFET LED driver

const int Sensor1 = 6;        // D in for PID sensor
const int Sensor2 = A2;        // A in for light sensor
const int Button1 = 7;        // D in for Bright+ switch
const int Button2 = 8;        // D in for Bright- switch
const int faderPot = A0;       // A in from 10k pot


    

int dim[5] = {0, 25,70, 100, 255};   // off, the three preset fade states, maximum
int dimLevel = 0;

// Variables

bool button1State = 0;      // the current "bright+ switch" state after the debounce
bool b1reading;                // debounce - the current reading from the button pin
bool b1previous = 0;         // debounce - the previous reading from the button pin

bool button2State = 0;      // the current "bright- switch" state after the debounce
bool b2reading;                // debounce - the current reading from the button pin
bool b2previous = 0;         // debounce - the previous reading from the button pin

int statusDisplayPeriod = 1000;  // interval for update display 
int statusDisplayTimer = 0;             // stores millis() to slow down reporting sensor state

bool sensor1State = 0;             // the current motion sensor state (0, 1)
bool sensor1Previous = 0;    // the previous state (for change detection)

bool motionTrigger = 0;          // if we are in a motion trigger event or not
int motionEnd;               // time that the light will switch off after a motion sense
int motionDuration;          // time in seconds to leave the light on
int previousDimLevel;        // the dim level before motion trigger

int sensor2Value = 0;        // the analog sensor value (0-255)
bool sensor2State = 0;             // the current daylight sensor state (0/1)
bool sensor2Previous = 0;    // the previous state (for change detection)
int sensor2Threshold = 512;   // the analog value set to trigger change (1 - 254)
int sensor2Hysteresis =0;        // raise to reduce flip-flopping (0 - 100)


int keypress = 0;      // has any input condition been met? used to envoke a fade (1,2,3,4)
int dayNightSampleTimer;       // time in millis/1000
int dayNightSamplePeriod = 5;  // time in seconds between samples of the sensor
int n = 15;                         // length of buffer
bool dayNightBuffer[15];         // when the buffer is full, a change is detected
                              // here: 120 seconds * 15 samples = 30 minutes
bool nightMode = 0;               // 1 when it is night time, night light comes on
bool dayNightBufferFull = 0;       // for day/night change confirmation
bool detectingTransition = 0; // flag to say we're watching for a day/night change

bool success = 0;

// for button debounce
long time = 0;                // the last time the output pin was toggled
long debounce = 200;          // the debounce time, increase if the output flickers


// for brightness and fading
int light1Bright = 0;          // Brightness of the output (PWM duty cycle)
int light1Target = 0;                // Based on conditions at the time of keypress

int light2Bright = 0;          // Brightness of the output (PWM duty cycle)
int light2Target = 0;                // Based on conditions at the time of keypress


int pot1Value = 10;            // this value used to slow the fade, read from faderPot
                               // it is actively read and updated during the fade
bool sign1 = 1;                  //for fade direction True = fade up.

int pot2Value = 10;            // this value used to set the mid level
int sign2 = 1;                  //used as the fade step -ve numbers fade down.


// for the blink_LED function
// int blinks;
// int onTime;
// int offTime;



void setup() {
    // LED pin definitions
    pinMode(statusLED, OUTPUT);
    pinMode(LEDpanel1, OUTPUT);
    pinMode(LEDpanel2, OUTPUT);
    pinMode(LEDpanel3, OUTPUT);

    // Light pin definitions
    pinMode(Light1, OUTPUT);
    pinMode(Light2, OUTPUT);
    
    // button and sensor pin definitions
    pinMode(Button1, INPUT_PULLUP);
    pinMode(Button2, INPUT_PULLUP);
    pinMode(Sensor1, INPUT_PULLUP);  // for digital sensor pulls to ground. 
                                    //This uses internal 20k pullup, inverts 0/1
    pinMode(Sensor2, INPUT);  // analog LDR daylight sensor
    
    Serial.begin(9600);
    Serial.print("Setup is done");
    
    // set up blink values and blink
//     blinks = 5;
//     onTime = 20;
//     offTime = 100;
    blink_LED(5, 20, 100);
  
}

void blink_LED(int blinks, int onTime, int offTime){
//     blinks, onTime, offTime
//     Serial.print(statusLED);
    for (byte i=0; i < blinks; i++){
        digitalWrite(statusLED, 1);
        Serial.print(" * ");
        delay(onTime);
        digitalWrite(statusLED, 0);
        delay(offTime);
    }
    
    
}

bool check_day_night_buffer(){
    Serial.print("buffer contents is :");
    dayNightBufferFull = 0;
    
    for (byte i=1; i <= sizeof dayNightBuffer; i++){
        // if the current sample does not match the previous, fail
        if (dayNightBuffer[i] == dayNightBuffer[i-1]){
            dayNightBufferFull = 1;
        }
        else{
            dayNightBufferFull = 0;
            
        }
        
        if (dayNightBuffer[i] == 1){
            Serial.print("O");
        }
        else{
            Serial.print("X");
        }
    }
    // if they all match, return the value
    if (dayNightBufferFull)
        Serial.print("Buffer Full");
    return dayNightBufferFull;
}

void display_status(){
    sensor2State = digitalRead(Sensor2);
    if (sensor2State == 0)
        Serial.print("-");
    else Serial.print("+");
}

int get_input(){
    Serial.println("Get Input");
    keypress=0;
    
    // end the triggered mode if timer is
    if (motionTrigger){
        if (millis()/1000 > motionEnd){
            keypress = 3;
        }
    }
    while (keypress == 0){
        b1reading = digitalRead(Button1);
        b2reading = digitalRead(Button2);

        // debounce tutor says;    
        // if the input just went from 0 and 1 and we've waited long enough
        // to ignore any noise on the circuit, toggle the output pin and remember
        // the time
            
        
        
        //check button1
        if (b1reading == 0 && b1previous == 1 && millis() - time > debounce) {
            if (button1State == 0)
                button1State = 1;
            else
                button1State = 0;
            keypress = 1;
            Serial.println(" B1trig ");
//             Serial.print(keypress);

            time = millis();
        }   

        //check button2
        if (b2reading == 0 && b2previous == 1 && millis() - time > debounce) {
            if (button2State == 0)
                button2State = 1;
            else
                button2State = 0;
            keypress = 2;
            Serial.println(" B2trig ");
//             Serial.print(keypress);

            time = millis();
        }   

        // finished checking the button inputs, now did the sensor see anythnig move since we last asked?
        
        sensor1Previous = sensor1State;
        sensor1State = digitalRead(Sensor1);
        if(sensor1State == 0 && sensor1Previous == 1){    
            Serial.print(" PID Sensor triggered ");
            keypress = 3;   
        }
        
    //         Serial.println(millis());
        // display a status line every x milliseconds
        if(millis() > statusDisplayTimer){
    //             Serial.print("Now  : ");
    //             Serial.println(millis());
            statusDisplayTimer = millis() + statusDisplayPeriod;
    //             Serial.print("Timer: ");
    //             Serial.println(statusDisplayTimer);
            display_status();
    //             Serial.print("done displaying status at: ");
    //             Serial.println(millis());
        }
            // is it time to check the daylight sensor?

        if (millis()/1000 > dayNightSampleTimer + dayNightSamplePeriod){
            dayNightSampleTimer = millis()/1000;
            Serial.print(sensor2State);
            Serial.print(" ~ ");
            sensor2Previous = sensor2State;
            Serial.println(sensor2Previous);
            sensor2Value = analogRead(Sensor2);
            Serial.print("Daylight sensor value: ");
            Serial.print(sensor2Value);
            if (sensor2Value > sensor2Threshold) sensor2State = 1;
            else sensor2State = 0;
            
            Serial.print(" & state : ");
            Serial.print(sensor2State);
            Serial.print("  Previously: ");
            Serial.println(sensor2Previous);
            
            if(sensor2Previous != sensor2State){ 
                Serial.print(" Daylight Sensor change detected!");
                detectingTransition = 1;
                // dayNightSampleTimer = millis()/1000;
                
                keypress = 4;
            }
            else{
                Serial.println("No Change");
            }
    //             sensor2Previous = sensor2State;
            
            // are we monitoring the sensor yet?
            if(detectingTransition){
                Serial.println("Detecting Transition...");
                // n = sizeof dayNightBuffer;
                //shift to the right (from stackexchange http://stackoverflow.com/questions/39546376/ddg#39546561)
                for (byte i=n-1;i>=0;i--){
                    dayNightBuffer[i+1] = dayNightBuffer[i]; //move all element to the right except last one
                }
                dayNightBuffer[0] = sensor2State; //assign current value to first element
                }
            }
        b1previous = b1reading;
        b2previous = b2reading;
    }
    return keypress;
}

bool do_fade(){
    
    Serial.print("  * start fade @");
    Serial.print(light1Bright);
    Serial.print(" to ");
    Serial.print(light1Target);
    Serial.print(" with fader @ ");
    Serial.print(pot1Value);
    
    // do the fade
    if(light1Bright >= light1Target){
        Serial.print(" + ");
        // fade up to new light1Target
        for(light1Bright; light1Bright <= light1Target; light1Bright++){
            // read the fader pot and increase/decrease brightness then delay per the pot
            //pot1Value = analogRead(faderPot);
            //pot1Value = map(pot1Value, 0, 1023, 1, 255);             // map range to useful min & max (1,255) 
            pot1Value = map(analogRead(faderPot), 0, 1023, 1, 255);
            analogWrite(Light1, light1Bright);                         // write brightness to pwm pin
            //             Serial.print(".");
            delay(pot1Value/10);                                         // slows the fade
            
        }
        
        
    }
    else{
        Serial.print(" - ");
        // fade down to new light1Target
        for(light1Bright; light1Bright <= light1Target; light1Bright--){
            // read the fader pot and increase/decrease brightness then delay per the pot
            //pot1Value = analogRead(faderPot);
            //pot1Value = map(pot1Value, 0, 1023, 1, 255);             // map range to useful min & max (1,255) 
            pot1Value = map(analogRead(faderPot), 0, 1023, 1, 255);
            analogWrite(Light1, light1Bright);                         // write brightness to pwm pin
            //             Serial.print(".");
            delay(pot1Value/10);                                         // slows the fade
            
        }
        
    }
    
    
    
    
    //kill the LED if PWM gets to zero
    if(light1Bright == 0){
        digitalWrite(Light1, 0);
        
    }
    
    Serial.print(" end loop @ ");
    Serial.print(light1Bright);  
    //      Serial.print(button1State);
    //      Serial.print("x");
    //      Serial.print(sensor1State);
    Serial.print("  ");
    return 1;
}

void loop(){
//     blink_LED(3, 200, 400);
    
    keypress = 0;
    digitalWrite(statusLED, 1);       //waiting for keypress
    digitalWrite(LEDpanel3, 0);
    Serial.println("start loop ");
//     Serial.print(millis()/1000);
//     Serial.print("  ");
//     Serial.println(dayNightSampleTimer);
    
    keypress = get_input();         // calls the get_input function
    
    Serial.print("exit from get_input ");
    Serial.print("keypress=");
    Serial.print(keypress);
    Serial.print(" ");
    Serial.print(",Sensor 1 = ");
    Serial.print(sensor1State);
    Serial.print("  ");
    Serial.print(",Sensor 2 = ");
    Serial.println(sensor2State);
    
    digitalWrite(statusLED, 0);      //key pressed - about to initiate a fade.But which kind?
    switch(keypress){
        case 1:
            if (dimLevel == 4) Serial.println("Already at maximum");
            else dimLevel++;
            Serial.print("Dim Level : ");
            Serial.println(dimLevel);
            blink_LED(dimLevel, 250, 100);
            light1Target = dim[dimLevel];
            digitalWrite(LEDpanel1, 1);
            digitalWrite(LEDpanel2, 0);
            digitalWrite(LEDpanel3, 0);
            break;
        
        case 2:
            if (dimLevel == 0) Serial.println("already off");
            else dimLevel--; 
            Serial.print("Dim Level : ");
            Serial.println(dimLevel);
            blink_LED(dimLevel, 250, 100);
            light1Target = dim[dimLevel];
            digitalWrite(LEDpanel1, 0);
            digitalWrite(LEDpanel2, 1);
            digitalWrite(LEDpanel3, 0);
            break;
            
        case 3:
            // The motion sensor triggered. Are we already in a motionTrigger mode?
            if (motionTrigger == 1){
                // check if its time to turn the light back down
                Serial.print("Checking Motion Timer @ ");
                Serial.println(millis()/1000);
                Serial.print("Motion event ending   @ ");
                Serial.println(motionEnd);
                if (millis()/1000 > motionEnd){
                    Serial.println("Time to end it");
                    dimLevel = previousDimLevel;
                    light1Target = dim[dimLevel];
                }
                else{
                    Serial.print("Still in motion trigger state for ");
                    Serial.print(motionEnd - millis()/1000);
                    Serial.println("more seconds");
                }
            }
            else {
                // new motion trigger event
                motionTrigger = 1;
                motionEnd = (millis()/1000) + 10;
                Serial.print("New Motion Trigger: end time = ");
                Serial.println(motionEnd);
                Serial.print("Setting dim level to ");
                previousDimLevel = dimLevel;
                dimLevel++;
                Serial.println(dimLevel);
                Serial.print("and begin fade to ");
                light1Target = dim[dimLevel];
                Serial.println(light1Target);
            }
            digitalWrite(LEDpanel1, 1);
            digitalWrite(LEDpanel2, 1);
            digitalWrite(LEDpanel3, 0);
            
            //pot1Value = analogRead(faderPot);
            break;
        case 4:
            digitalWrite(LEDpanel1, 0);
            digitalWrite(LEDpanel2, 0);
            digitalWrite(LEDpanel3, 1);

            // check if the hysteresis buffer is full
            dayNightBufferFull = check_day_night_buffer();
            if(dayNightBufferFull){
                nightMode = dayNightBuffer[0];
                // either fade on or off
                if (nightMode){
                    light1Target = 0;
                }
                else{
                    light1Target = 128;
                }
                //  exit the detecting mode, clear the buffer
                detectingTransition = 0;
                dayNightBufferFull = 0;
                Serial.print("dayNightBuffer = [");
                for (byte i = 0; i < n; i++){
                    if (i != 0){
                        Serial.print(",");
                    }
                    Serial.print(dayNightBuffer[i]);
                    dayNightBuffer[i] = "";
                }
                Serial.println("]");
            }
            break;
    }
    success = do_fade();
    Serial.print("Fade status: ");
    Serial.println(success);
}
